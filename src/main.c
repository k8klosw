/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/types.h>

#include <linux/input.h>
#include <linux/kd.h>

#include <X11/Xlib.h>

#include "xkbutils.h"


/*
  {"LEFTCTRL", 29},
  {"RIGHTCTRL", 97},
  {"LEFTSHIFT", 42},
  {"RIGHTSHIFT", 54},
  {"LEFTALT", 56},
  {"RIGHTALT", 100},
  {"LEFTMETA", 125},
  {"RIGHTMETA", 126},
  {"CONTEXTMENU", 127},
*/


#define KEY_CONTEXTMENU  127

#define MAGIC_COUNT  3
static int magic_codes[MAGIC_COUNT] = {
  /* the whole right part */
  97, /* right ctrl */
  126, /* right hyper */
  127, /* context menu */
};


////////////////////////////////////////////////////////////////////////////////
static char *devName = NULL;
static int optDontDaemonize = 0;
static int optMute = 0;
static int optSameSound = 0;


////////////////////////////////////////////////////////////////////////////////
// ������������� �������������� ������� ��� ���������� � ���������
typedef struct {
  int type; // 1: kbd
  int code; // key
  int action; // 0: release; 1: press; 2: repeat
} UniEvent;


static void readKdbEvent (int fd, UniEvent *ev) {
  ssize_t rb;
  struct input_event kev;
  rb = read(fd, &kev, sizeof(kev));
  if (rb != (ssize_t)sizeof(kev)) {
    fprintf(stderr, "FATAL: invalid keyboard event\n");
    exit(1);
  }
  ev->type = kev.type;
  ev->code = kev.code;
  ev->action = kev.value;
}


////////////////////////////////////////////////////////////////////////////////
static int openEvDev (Display *dpy) {
  int fd;
  if (!devName) {
    fprintf(stderr, "FATAL: no input device specified\n");
    exit(1);
  }
  if ((fd = open(devName, O_RDONLY)) < 0) {
    fprintf(stderr, "FATAL: can't open input device: '%s'\n", devName);
    exit(1);
  }
  return fd;
}


////////////////////////////////////////////////////////////////////////////////
typedef int (*LoopFn) (UniEvent *ev);

static void runMainLoop (Display *dpy, LoopFn fn) {
  int fd = openEvDev(dpy);
  if (!optDontDaemonize) {
    if (daemon(0, 0) != 0) {
      fprintf(stderr, "FATAL: can't daemonize\n");
      exit(1);
    }
  }
  for (;;) {
    UniEvent ev;
    readKdbEvent(fd, &ev);
    if (ev.type == 1) {
      if (fn(&ev)) return;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
static void testEvents (Display *dpy) {
  int eventCount = 1;
  int cb (UniEvent *ev) {
    printf("[%d]: %s %d\n", eventCount,
      (ev->action == 0 ? "up  " : (ev->action == 1 ? "down" : (ev->action == 2 ? "rept" : "????"))),
      ev->code);
    ++eventCount;
    return 0;
  }
  printf("event code test; ^C for exit\n");
  optDontDaemonize = 1;
  runMainLoop(dpy, cb);
}


////////////////////////////////////////////////////////////////////////////////
static void xsleep (int ms) {
  if (ms <= 0) {
    usleep(1);
  } else {
    if (ms/1000 > 0) sleep(ms/1000);
    ms *= 1000;
    usleep((useconds_t)ms);
  }
}


static void beep (int hz, int waitms) {
  if (hz > 10 || hz < 20000) {
    int fd = open("/dev/console", O_WRONLY);
    if (fd >= 0) {
      ioctl(fd, KIOCSOUND, (int)(/*CLOCK_TICK_RATE*/1193180/hz));
      xsleep(waitms);
      ioctl(fd, KIOCSOUND, 0);
      close(fd);
    }
  }
}


static uint64_t getmtime (void) {
  struct timespec tp;
  uint64_t res;
  clock_gettime(CLOCK_MONOTONIC, &tp); //1e-9
  res = ((uint64_t)tp.tv_sec)*1000LLU+((uint64_t)tp.tv_nsec)/1000000LLU;
  return res;
}


////////////////////////////////////////////////////////////////////////////////
/*
#define MAGIC_KEY1  KEY_RIGHTCTRL
#define MAGIC_KEY2  KEY_CONTEXTMENU
*/

static int isMagicCode (int c) {
  for (unsigned f = 0; f < MAGIC_COUNT; ++f) {
    if (magic_codes[f] == c) return 1;
  }
  return 0;
}


// action code: 0:not pressed; 1:just pressed; 2:repeat
enum {
  KEY_RELEASED = 0,
  KEY_PRESSED = 1,
  KEY_REPEAT = 2,
};

static unsigned char pressed[512];
static uint64_t downTime = 0;
static Display *maindpy = NULL;


static void resetAllMagics (void) {
  for (unsigned f = 0; f < MAGIC_COUNT; ++f) {
    pressed[magic_codes[f]] = KEY_RELEASED;
  }
}


static int isOtherMagicDown (int ckmagic) {
  for (unsigned f = 0; f < MAGIC_COUNT; ++f) {
    if (magic_codes[f] == ckmagic) continue;
    if (pressed[magic_codes[f]]) return 1;
  }
  return 0;
}


static int isSmthDown (void) {
  for (unsigned f = 0; f < 512; ++f) {
    if (isMagicCode(f)) continue;
    if (pressed[f] != KEY_RELEASED) return 1;
  }
  return 0;
}


static int cb (UniEvent *ev) {
  if (ev->code < 1 || ev->code > 511 || ev->action < 0 || ev->action > 2) return 0;
  //
  /*
  printf("%s %d\n",
    (ev->action == 0 ? "up  " : (ev->action == 1 ? "down" : (ev->action == 2 ? "rept" : "????"))),
    ev->code);
  */
  if (isMagicCode(ev->code)) {
    // if something else is pressed, abort switching
    if (isSmthDown() || isOtherMagicDown(ev->code)) {
      resetAllMagics();
      return 0;
    }
    switch (ev->action) {
      case KEY_RELEASED:
        if (pressed[ev->code] == KEY_PRESSED) {
          resetAllMagics();
          // there were no repeats or other activity
          // check timeout
          //fprintf(stderr, "%lld\n", getmtime()-downTime);
          if (getmtime()-downTime < 400) {
            int c = activeLayout(maindpy)+1;
            if (c >= layoutCount(maindpy)) c = 0;
            setActiveLayout(maindpy, c);
            if (!optMute) beep(440+c*80*(optSameSound ? 0 : 1), 30);
          }
          return 0;
        }
        break;
      case KEY_PRESSED:
        downTime = getmtime();
        break;
      case KEY_REPEAT:
        if (pressed[ev->code] == KEY_PRESSED) return 0;
        break;
    }
  } else {
    // some other keyboard activity, prevent switching
    resetAllMagics();
  }
  pressed[ev->code] = ev->action;
  return 0;
}


static void mainLoop (Display *dpy) {
  memset(pressed, KEY_RELEASED, sizeof(pressed));
  maindpy = dpy;
  downTime = 0;
  runMainLoop(dpy, &cb);
}


////////////////////////////////////////////////////////////////////////////////
static int findKeyboard (void) {
  for (int f = 0; f <= 31; ++f) { // the Elder says there canst be no other numbers
    char dev[32];
    int fd;
    sprintf(dev, "/dev/input/event%d", f);
    fd = open(dev, O_RDONLY);
    if (fd >= 0) {
      char name[128];
      int len = ioctl(fd, EVIOCGNAME(sizeof(name)-1), name);
      close(fd);
      if (len > 0) {
        name[len] = 0;
        if (strcasestr(name, "keyboard")) {
          devName = strdup(dev);
          return 1;
        }
      }
    }
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
static void showHelp (void) {
  printf(
    "-l   list layouts\n"
    "-e   event dumper\n"
    "-D   do not daemonize\n"
    "-d   device_name\n"
    "-m   mute\n"
    "-s   same sound for all layouts\n"
  );
}


////////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
  Display *dpy;
  int action = 0;
  for (int f = 1; f < argc; ++f) {
    const char *arg = argv[f];
    if (arg[0] == '-' && arg[1] == '-') {
      if (strcmp(arg, "--help") == 0) { showHelp(); return 0; }
      goto badarg;
    } else if (arg[0] == '-') {
      for (int c = 1; arg[c]; ++c) {
        switch (arg[c]) {
          case 'h': showHelp(); return 0;
          case 'l': action |= 2; break;
          case 'e': action |= 1; break;
          case 'D': optDontDaemonize = 1; break;
          case 'm': optMute = 1; break;
          case 's': optSameSound = 1; break;
          case 'd':
            ++f;
            if (f >= argc) { fprintf(stderr, "FATAL: '-d' expects device name\n"); return 1; }
            if (devName) free(devName);
            devName = strdup(argv[f]);
            break;
          default: fprintf(stderr, "FATAL: invalid option: '%c'\n", arg[c]); return 1;
        }
      }
    } else {
badarg:
      fprintf(stderr, "FATAL: invalid argument: '%s'\n", arg);
      return 1;
    }
  }
  dpy = XOpenDisplay(NULL);
  if (dpy == NULL) {
    fprintf(stderr, "FATAL: unable to connect to X server\n");
    return 1;
  }
  if (action&2) {
    printLayouts(dpy);
    return 0;
  }
  if (!devName) {
    if (!findKeyboard()) { fprintf(stderr, "FATAL: can't find keyboard device\n"); return 1; }
    printf("keyboard device: %s\n", devName);
  }
  if (action&1) testEvents(dpy); else mainLoop(dpy);
  return 0;
}
