/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 */
#include <fcntl.h>
#include <glob.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <linux/input.h>
#include <sys/ioctl.h>
#include <sys/types.h>


static void listInputNames (void) {
  glob_t g;
  memset(&g, 0, sizeof(g));
  if (glob("/dev/input/event*", GLOB_MARK, NULL, &g) == 0) {
    for (int f = 0; f < g.gl_pathc; ++f) {
      const char *dev = g.gl_pathv[f];
      //
      if (dev[strlen(dev)-1] != '/') {
        int fd = open(dev, O_RDONLY);
        if (fd >= 0) {
          char name[512];
          int len = ioctl(fd, EVIOCGNAME(sizeof(name)-1), name);
          if (len > 0) {
            name[len] = 0;
            printf("%s : %s\n", dev, name);
          }
          close(fd);
        }
      }
    }
  }
  globfree(&g);
}


int main () {
  listInputNames();
  return 0;
}
