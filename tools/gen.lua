--#define KEY_

local fi = assert(io.open("/usr/include/linux/input.h", "r"));

local keys = {};
for s in fi:lines() do
  local n, v = s:match("^%s*#%s*define%s+KEY_(%S+)%s+(0x[0-9A-Fa-f]+)");
  if not v then
    n, v = s:match("^%s*#%s*define%s+KEY_(%S+)%s+(%d+)");
  end;
  if n and v and n ~= "MAX" then
    --print(n, v);
    assert(tonumber(v));
    keys[#keys+1] = { name=n, value = tonumber(v); }
  end;
end;
fi:close();


local fo = assert(io.open("keynames.c", "w"));
fo:write('#include "keynames.h"\n\n');
fo:write("KeyInfo keynames[] = {\n");
for _, k in ipairs(keys) do
  fo:write(string.format('  {"%s", %d},\n', k.name, k.value));
end;
fo:write("  {0, -1}\n};\n");
fo:close();
